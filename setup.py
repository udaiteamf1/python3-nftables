from setuptools import setup

setup(name='nftables',
      version='0.1',
      description='Dump python nftables',
      url='http://github.com/storborg/funniest',
      author='Flying Circus',
      author_email='flyingcircus@example.com',
      license='MIT',
      packages=['nftables'],
      zip_safe=False)